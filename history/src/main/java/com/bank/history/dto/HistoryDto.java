package com.bank.history.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.Nullable;



@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HistoryDto {

    private Long id;

    @Nullable
    private Long transfer_audit_id;

    @Nullable
    private Long profile_audit_id;

    @Nullable
    private Long account_audit_id;

    @Nullable
    private Long anti_fraud_audit_id;

    @Nullable
    private Long public_bank_info_audit_id;

    @Nullable
    private Long authorization_audit_id;
}
