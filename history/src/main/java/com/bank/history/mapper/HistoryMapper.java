package com.bank.history.mapper;

import com.bank.history.dto.HistoryDto;
import com.bank.history.entity.History;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface HistoryMapper {
    HistoryMapper MAPPER = Mappers.getMapper(HistoryMapper.class);

    HistoryDto fromHistory(History history);

    History toHistory(HistoryDto historyDto);

    List<HistoryDto> fromHistoryList(List<History> histories);
}
