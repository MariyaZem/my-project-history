package com.bank.history.entity;

import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.*;

@Entity
@Table(name = "history", schema = "history")
@Data
public class History {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Nullable
    @Column(name="transfer_audit_id")
    private Long transfer_audit_id;

    @Nullable
    @Column(name="profile_audit_id")
    private Long profile_audit_id;

    @Nullable
    @Column(name="account_audit_id")
    private Long account_audit_id;

    @Nullable
    @Column(name="anti_fraud_audit_id")
    private Long anti_fraud_audit_id;

    @Nullable
    @Column(name="public_bank_info_audit_id")
    private Long public_bank_info_audit_id;

    @Nullable
    @Column(name="authorization_audit_id")
    private Long authorization_audit_id;

}
