package com.bank.history.controller;

import com.bank.history.dto.HistoryDto;
import com.bank.history.entity.History;
import com.bank.history.mapper.HistoryMapper;
import com.bank.history.service.HistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/histories")
public class HistoryController {

    private final HistoryService historyService;


    @GetMapping
    public ResponseEntity<List<HistoryDto>> getAllHistory() {
        List<History> historyList = historyService.getAllHistory();
        List<HistoryDto> dtoList = HistoryMapper.MAPPER.fromHistoryList(historyList);
        return new ResponseEntity<>(dtoList, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<HistoryDto> getHistoryById(@PathVariable ("id") Long id) {
        History history = historyService.getHistoryById(id);
        HistoryDto historyDto = HistoryMapper.MAPPER.fromHistory(history);
        return ResponseEntity.ok().body(historyDto);
    }

    @PostMapping
    public ResponseEntity<HttpStatus> addHistory(@RequestBody HistoryDto historyDto) {
        History history = HistoryMapper.MAPPER.toHistory(historyDto);
        historyService.createHistory(history);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<HttpStatus> updateHistory(@PathVariable("id") Long id, @RequestBody HistoryDto historyDto) {
        History history = HistoryMapper.MAPPER.toHistory(historyDto);
        historyService.updateHistory(id, history);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteHistory(@PathVariable("id") Long id) {
        historyService.deleteHistory(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    }
