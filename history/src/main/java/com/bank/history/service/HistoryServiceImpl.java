package com.bank.history.service;

import com.bank.history.entity.History;
import com.bank.history.exeption.HistoryNotFoundException;
import com.bank.history.repository.HistoryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class HistoryServiceImpl implements HistoryService{
    private final HistoryRepository historyRepository;


   @Override
   public List<History> getAllHistory() {
        return historyRepository.findAll();
   }

    @Override
    public History getHistoryById(Long id) {
        Optional<History> foundHistory =  historyRepository.findById(id);
        return foundHistory.orElseThrow(HistoryNotFoundException::new);
    }

    @Transactional
    @Override
    public History createHistory(History history) {
        historyRepository.save(history);
        return history;
    }

    @Transactional
    @Override
    public void deleteHistory(Long id) {
        historyRepository.deleteById(id);
    }

    @Transactional
    @Override
    public void updateHistory(Long id, History updatedHistory) {
        getHistoryById(id);
        historyRepository.save(updatedHistory);
    }
}
