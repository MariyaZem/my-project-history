package com.bank.history.service;

import com.bank.history.entity.History;

import java.util.List;

public interface HistoryService {
    History createHistory(History history);
    List<History> getAllHistory();
    History getHistoryById(Long id);

    void deleteHistory(Long id);
    void updateHistory(Long id, History updatedHistory);
}
