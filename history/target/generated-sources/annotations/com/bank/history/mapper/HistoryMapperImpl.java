package com.bank.history.mapper;

import com.bank.history.dto.HistoryDto;
import com.bank.history.entity.History;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-03-11T16:12:25+0300",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 19.0.2 (Oracle Corporation)"
)
@Component
public class HistoryMapperImpl implements HistoryMapper {

    @Override
    public HistoryDto fromHistory(History history) {
        if ( history == null ) {
            return null;
        }

        HistoryDto historyDto = new HistoryDto();

        historyDto.setId( history.getId() );
        historyDto.setTransfer_audit_id( history.getTransfer_audit_id() );
        historyDto.setProfile_audit_id( history.getProfile_audit_id() );
        historyDto.setAccount_audit_id( history.getAccount_audit_id() );
        historyDto.setAnti_fraud_audit_id( history.getAnti_fraud_audit_id() );
        historyDto.setPublic_bank_info_audit_id( history.getPublic_bank_info_audit_id() );
        historyDto.setAuthorization_audit_id( history.getAuthorization_audit_id() );

        return historyDto;
    }

    @Override
    public History toHistory(HistoryDto historyDto) {
        if ( historyDto == null ) {
            return null;
        }

        History history = new History();

        history.setId( historyDto.getId() );
        history.setTransfer_audit_id( historyDto.getTransfer_audit_id() );
        history.setProfile_audit_id( historyDto.getProfile_audit_id() );
        history.setAccount_audit_id( historyDto.getAccount_audit_id() );
        history.setAnti_fraud_audit_id( historyDto.getAnti_fraud_audit_id() );
        history.setPublic_bank_info_audit_id( historyDto.getPublic_bank_info_audit_id() );
        history.setAuthorization_audit_id( historyDto.getAuthorization_audit_id() );

        return history;
    }

    @Override
    public List<HistoryDto> fromHistoryList(List<History> histories) {
        if ( histories == null ) {
            return null;
        }

        List<HistoryDto> list = new ArrayList<HistoryDto>( histories.size() );
        for ( History history : histories ) {
            list.add( fromHistory( history ) );
        }

        return list;
    }
}
